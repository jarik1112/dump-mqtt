import { connect } from 'mqtt';
import { MqttMessage } from './types';
import { promises, writeFileSync } from 'fs';
import { resolve } from 'path';
import { config } from 'dotenv';
config();

const { stat, mkdir, writeFile } = promises;

const maxMessages = 100;
const dumpDirrectory = resolve(process.cwd(), process.env.DUMP_DIR || './');

const messages: MqttMessage[] = [];

async function saveDumps() {
  if (messages.length > 0) {
    await writeFile(
      resolve(dumpDirrectory, `${Date.now()}.json`),
      JSON.stringify(messages.splice(0)),
    );
  }
}

function syncSaveDumps() {
  console.log(`Dumps to save: ${messages.length}`);
  if (messages.length > 0) {
    writeFileSync(
      resolve(dumpDirrectory, `${Date.now()}.json`),
      JSON.stringify(messages.splice(0), null, 2),
    );
  }
}

async function pushMessage(msg: MqttMessage): Promise<void> {
  messages.push(msg);
  if (messages.length >= maxMessages) {
    await saveDumps();
  }
}

// Save on exit
process.on('SIGINT', () => {
  syncSaveDumps();
  process.exit();
});
process.on('exit', () => syncSaveDumps());

async function run() {
  // Init dump dir
  try {
    await stat(dumpDirrectory);
  } catch (error) {
    await mkdir(dumpDirrectory);
  }

  const client = connect(`mqtt://${process.env.MQTT_URL}`, {
    username: process.env.MQTT_USER,
    password: process.env.MQTT_PASS,
  });

  console.log(client.connected);

  client.on('error', (error) => {
    console.error(`Client error`, error);
  });

  client.on('connect', () => {
    console.log('Connected', client.connected);
    client.subscribe('#', function (err) {
      if (err) {
        console.error(err);
      }
    });
    client.subscribe('$SYS', function (err) {
      if (err) {
        console.error(err);
      }
    });
  });

  client.on('message', (topic, message) => {
    console.log(topic, message.toString('utf-8'));
    pushMessage({ topic, message: message.toString('utf-8') });
  });

  // Start timeout dump
  setTimeout(() => saveDumps(), 60 * 1000);

  process.on('exit', () => client.end());
}

run()
  .then(() => {
    console.log('App runnig');
  })
  .catch((error) => {
    console.log(error);
  });

export type MqttMessage = {
  topic: string;
  message: string;
};
